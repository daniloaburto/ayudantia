# Dockerfile construido en base a: (2)
# https://github.com/dockerfiles/django-uwsgi-nginx
# Cambio hecho por user2
from ubuntu:14.04
# Cambio agregado por user1
maintainer Requies
# Cambio agregado por user2 (2)
# Instalacion de dependencias base
run apt-get update
run apt-get install -y build-essential git
run apt-get install -y python python-dev python-setuptools
run apt-get install -y nginx supervisor
run apt-get install -y libpq-dev

run easy_install pip

# install uwsgi now because it takes a little while
run pip install uwsgi

# Install nodejs
run apt-get install -y python-software-properties
run wget -sL https://deb.nodesource.com/setup | bash -
run apt-get install -y nodejs
run apt-get install -y npm
run ln -s /usr/bin/nodejs /usr/bin/node

# Install app dependencies
add server-config /home/docker/code/server-config/
add django-project/package.pip /home/docker/code/django-project/
run pip install `grep -v -e '^\#' -e '^\s*$$' /home/docker/code/django-project/package.pip`

# setup all the configfiles
run echo "daemon off;" >> /etc/nginx/nginx.conf
run rm /etc/nginx/sites-enabled/default
run ln -s /home/docker/code/server-config/nginx-app.conf /etc/nginx/sites-enabled/
run ln -s /home/docker/code/server-config/supervisor-app.conf /etc/supervisor/conf.d/

# install our code and remove local settings if exists
add . /home/docker/code/
run rm -f /home/docker/code/django-project/config/local_settings.py

# Build web application
#######################

# Run makefile inside project in order to build it
run make --directory /home/docker/code/django-project/

expose 85
